//
//  MainViewController.swift
//  cleanswift2
//  Created by Артур on 16.01.2022.

import UIKit

class MainViewController: UIViewController, MainDisplayLogic {
    //MARK: -Internal properties
    var interactor: MainBusinessLogic? = nil
    var router: MainRoutingLogic? = nil

    let buthon: UIButton = {
        let but = UIButton()
        but.frame = CGRect(x: 45, y: 700, width: 300, height: 70)
        but.backgroundColor = .blue
        but.titleLabel?.textAlignment = .center
        but.layer.cornerRadius = 20
        but.setTitleColor(.red, for: .normal)
        but.setTitleColor(.gray, for: .highlighted)
        but.addTarget(self, action: #selector(alertContrMethod), for: .touchUpInside)
        return but
    }()
    
    var image: UIImageView = {
        let imags = UIImageView()
        imags.frame = CGRect(x: 50, y: 200, width: 300, height: 300)
        imags.clipsToBounds
        return imags
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(buthon)
        view.addSubview(image)
        interactor?.loadTitleData(with: MainModelScene.TitleData.Request())
    }

    func displayTitleData(with view: Model.TitleData.ViewModel) {
        buthon.setTitle(view.labelButonNormal, for: .normal)
        buthon.setTitle(view.labelButonHiglaited, for: .highlighted)
        image.image = view.images.imageFlippedForRightToLeftLayoutDirection()
    }
    
    @objc func alertContrMethod() {
        let alertController = UIAlertController(title: "Продолжить", message: "", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Я Богатый", style: .default) { (alertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        let alertActionTwo = UIAlertAction(title: "Я Бедолага", style: .destructive) { (alertActionTwo) in
          self.router?.routeTu()
        }
        
        alertController.addAction(alertAction)
        alertController.addAction(alertActionTwo)
        self.present(alertController, animated: true, completion: nil)
    }
}


