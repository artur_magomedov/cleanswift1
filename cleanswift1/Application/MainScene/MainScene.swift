//
//  MainScene.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import UIKit

enum MainScene {
    static func build() -> UIViewController {
        let interactor = MainInteractor()
        let presenter = MainPresenter()
        let viewController = MainViewController()
        let router = MainRouter()
        let worker = MainWorker()
  
        interactor.presenter = presenter
        interactor.worker = worker
        
        presenter.viewController = viewController
       
        viewController.router = router
        viewController.interactor = interactor
        
        router.viewController = viewController
        router.dataStore = interactor
        
        return viewController
    }
}
