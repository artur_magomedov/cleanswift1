//
//  MainModel.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import UIKit

enum MainModelScene {
    enum TitleData {
        struct Request { }
        struct Response {
            let labelButonNormal: String
            let labelButonHiglaited: String
            let images: UIImage
        }
        typealias ViewModel = Response
    }
    
    enum Error: Swift.Error {
        case NetworkError(String)
    }
}
