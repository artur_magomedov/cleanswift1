//
//  MainPresenter.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.

import UIKit

final class MainPresenter: MainPresentationLogic {
    //MARK: -Internal properties
   weak var viewController: MainDisplayLogic?
    
    func presentationTitleData(with response: Model.TitleData.Response) {
        let title = response.images.images
        let vieModel = MainModelScene.TitleData.ViewModel(labelButonNormal: "Получить", labelButonHiglaited: "Dollars", images: UIImage(named:"original.png")!)
        viewController?.displayTitleData(with: vieModel)
    }
}
