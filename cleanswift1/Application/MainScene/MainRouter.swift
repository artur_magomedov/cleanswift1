//
//  MainRouter.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import UIKit

final class MainRouter: MainRoutingLogic,
                        MainDataPassing {
    // MARK: - MainDataPassing
    var dataStore: MainDataStore?
    
   weak var viewController: UIViewController? 
    
    func routeTu() {
        guard let title = dataStore?.titles else { return }
        DispatchQueue.main.async {
            let scene = SecondScenes.builds(config: MainTwoInteractor.Config(titles: title))
            self.viewController?.navigationController?.pushViewController(scene, animated: true)
        }
    }
}
