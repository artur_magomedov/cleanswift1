//
//  MainInteractor.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
import UIKit

final class MainInteractor: MainBusinessLogic,
                            MainDataStore {
    //MARK: -MainDataStore
    var titles: UIImage?
    
    //MARK: -Internal properties
    var presenter: MainPresentationLogic?
    var worker: MainWorkingLogic?
   
    func loadTitleData(with request: Model.TitleData.Request) {
       worker?.fetchData(with: request) { [weak presenter, weak self] result in
            switch result {
            case .success(let response):
                presenter?.presentationTitleData(with: response)
                self?.titles = response.images.imageFlippedForRightToLeftLayoutDirection()
            case .failure(let error):
                print(error)
            }
        }
    }
}

