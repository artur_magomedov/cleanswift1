//
//  MainProtocol.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//
import UIKit

protocol MainTypalias {
    typealias Model = MainModelScene
}

protocol MainBusinessLogic: MainTypalias, AnyObject {
    func loadTitleData(with request: Model.TitleData.Request)
}

protocol MainPresentationLogic: MainTypalias, AnyObject {
    func presentationTitleData(with response: Model.TitleData.Response)
}

protocol MainDisplayLogic: MainTypalias, AnyObject {
    func displayTitleData(with view: Model.TitleData.ViewModel)
    
}

protocol MainDataPassing: MainTypalias, AnyObject {
    var dataStore: MainDataStore? { get }
}

protocol MainDataStore: MainTypalias, AnyObject {
    var titles: UIImage? {get set }
}

protocol MainRoutingLogic {
    func routeTu()
}

protocol MainWorkingLogic: MainTypalias, AnyObject {
    func fetchData(with request: Model.TitleData.Request, complition: @escaping(Result<Model.TitleData.Response, Model.Error>) -> Void)
}

