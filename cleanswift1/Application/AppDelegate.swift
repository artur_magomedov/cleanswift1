//
//  AppDelegate.swift
//  cleanswift1
//
//  Created by Артур on 20.01.2022г

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        self.window?.rootViewController = UINavigationController(rootViewController: MainScene.build())
        self.window?.makeKeyAndVisible()

        return true
    }
}

