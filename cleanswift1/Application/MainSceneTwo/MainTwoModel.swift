//
//  MainTwoModel.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import UIKit

enum SecondModel {
    enum Data {
        struct Request { }
        struct Response {
            let title: UIImage
            let label: String
        }
       typealias ViewModel = Response
    }
}
