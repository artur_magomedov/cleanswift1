//
//  MainTwoInteractor.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import UIKit

final class MainTwoInteractor: SecondBusinessLogic {
    // MARK: - Models
    struct Config {
        let titles: UIImage
    }
    
    //MARK: -Internal properties
    var presenters: SecondPresentationlogic?
    var config: Config
    
    // MARK: - Initialization
    init(config: Config) {
        self.config = config
    }
    
    func loadData(with request: Models.Data.Request) {
        presenters?.presentationData(with: Models.Data.Response(title: config.titles, label: "Отгадать загадку?"))
    }
}
