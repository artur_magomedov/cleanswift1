//
//  MainTwoScene.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import UIKit

enum SecondScenes {
    static func builds(config: MainTwoInteractor.Config) -> UIViewController {
        
        let interact = MainTwoInteractor(config: config)
        let present = MainTwoPresenter()
        let viewVC = MainViewControllerTwo()
        let  rout = MainTwoRouter()
        
        interact.presenters = present
        
        present.viewControlers = viewVC
        
        viewVC.interactor = interact
        viewVC.router = rout
        rout.viewsVC = viewVC
    
        return viewVC
    }
}
