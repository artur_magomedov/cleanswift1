//
//  MainTwoProtocol.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import Foundation

protocol Typalies {
    typealias Models = SecondModel
}

protocol SecondBusinessLogic: Typalies {
   func loadData(with request: Models.Data.Request)
}

protocol SecondPresentationlogic: Typalies {
    func presentationData(with response: Models.Data.Response )
}

protocol SecondDisplayLogic: Typalies, AnyObject {
    func displayData(with view: Models.Data.ViewModel)
}

protocol SecondRoutingLogic {
}
