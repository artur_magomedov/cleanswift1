//
//  MainTwoPresenter.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import UIKit

final class MainTwoPresenter: SecondPresentationlogic {
    //MARK: -Internal properties
    var viewControlers: SecondDisplayLogic?
    
    func presentationData(with response: Models.Data.Response) {
        viewControlers?.displayData(with: response)
    }
}
