//
//  MainViewControllerTwo.swift
//  cleanswift2
//
//  Created by Артур on 16.01.2022.
//

import UIKit

class Words {
    var word = String()
    var newWords = String()
    
    func compair(word: String) {
        if word == "Огурец" {
            newWords = "ИДИ РАБОТАЙ!!!!!"
        } else {
            newWords = "Неверно! Cлово с большой буквы."
        }
    }
}

class MainViewControllerTwo: UIViewController, SecondDisplayLogic {
    //MARK: -Internal properties
    var interactor: SecondBusinessLogic?
    var router: SecondRoutingLogic?
    let yourText = Words()

    var imag: UIImageView = {
        let imags = UIImageView()
        imags.frame = CGRect(x: 0, y: 0, width: 1000, height: 900)
        imags.clipsToBounds
        return imags
    }()
    
    var lable: UILabel = {
        let labs = UILabel()
        labs.frame = CGRect(x: 50, y: 380, width: 300, height: 100)
        labs.textColor = .red
        labs.backgroundColor = .white
        labs.textAlignment = .center
            return labs
    }()
    
    var but: UIButton = {
        let buth = UIButton()
        buth.frame = CGRect(x: 40, y: 200, width: 300, height: 200)
        buth.setTitleColor(.red, for: .normal)
        buth.setTitleColor(.darkGray, for: .highlighted)
        buth.addTarget(self, action: #selector(alertGame), for: .touchUpInside)
        return buth
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        interactor?.loadData(with: SecondModel.Data.Request())
        view.addSubview(imag)
        view.addSubview(but)
        view.addSubview(lable)
    }
    
    @objc func alertGame() {
        let alertController = UIAlertController(title: "Загадка", message: "На полу валяется ни кто его не ест!", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ок", style: .destructive) { (action) in
            let text = alertController.textFields?.first
             self.yourText.word = text!.text!
            
            self.yourText.compair(word: self.yourText.word)
            self.lable.text = self.yourText.newWords
        }
        alertController.addTextField(configurationHandler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func displayData(with view: Models.Data.ViewModel) {
       imag.image = view.title.imageFlippedForRightToLeftLayoutDirection()
        but.setTitle(view.label, for: .normal)
        but.setTitle(view.label, for: .highlighted)
    }
}


